site_name = 'iceberg'

command = '/srv/{}/venv/bin/gunicorn'.format(site_name)
python = '/srv/{}'.format(site_name)
bind = '127.0.0.1:8002'

preload_app = True
workers = 3
