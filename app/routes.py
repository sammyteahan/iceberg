from flask import render_template

from app import app


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/menu', methods=['GET'])
def menu():
    return render_template('menu.html')
