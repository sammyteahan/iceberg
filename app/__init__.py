import os

from flask import Flask

from config import DevelopmentConfig, ProductionConfig

app = Flask(__name__)

if os.environ.get('FLASK_ENV') == 'production':
    app.config.from_object(ProductionConfig)
else:
    app.config.from_object(DevelopmentConfig)

# flask struggles with circular imports,
# this setup helps that
# pylint: disable=C0413
from app import routes
