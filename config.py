import os
from os.path import join, dirname

from dotenv import load_dotenv

env_path = join(dirname(__file__), '.env')
load_dotenv(env_path)


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY', 'sample-key')


class DevelopmentConfig(Config):
    SCHEME = 'http'
    FLASK_DEBUG = 1


class ProductionConfig(Config):
    SCHEME = 'https'
    FLASK_DEBUG = 0
